#!/bin/bash

set -x

check_return_value () {
    if [ $1 != 0 ] ; then
        exit $1
    fi
}

cd $1
./configure --prefix=/usr --disable-opt --enable-lzo --enable-lzma
check_return_value $?
make
check_return_value $?
make check
exit $?
